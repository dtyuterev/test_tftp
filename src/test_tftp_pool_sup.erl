-module(test_tftp_pool_sup).

-behaviour(supervisor).

%% API
-export([
	start_link/0,
	start_child/1
]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, temporary, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    %Settings = [],
    %lager:debug("sss Args getted : ~p",[Settings]),
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    Child = ?CHILD(test_tftp_pool_worker, worker),
    {ok, {{simple_one_for_one, 4, 3600}, [Child]}}.

start_child(Name) ->
    lager:debug("Start child ~p",[Name]),
    supervisor:start_child(?MODULE, [Name]).

