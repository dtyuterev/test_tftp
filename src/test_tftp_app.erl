-module(test_tftp_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    [ application:ensure_all_started(App) || App <- [lager,gproc]],
    lager:debug("Let's begin"),
    test_tftp_sup:start_link().

stop(_State) ->
    ok.
