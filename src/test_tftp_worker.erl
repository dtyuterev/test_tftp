-module(test_tftp_worker).
-behaviour(gen_server).
-define(SERVER, ?MODULE).
-define(L_PORT, 5888).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([
	start_link/0 
	%,send/1
]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

-record(state,{
	socket = undefined,
	curr_file = undefined
}).
%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------
init(_Args) ->
    self()!init,
    {ok, #state{}}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%handle_cast({send,{IP, Port, D}}, #state{socket = S} = State) ->
%    gen_udp:send(S,IP,Port,D),
%    {noreply, State};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(init, State) ->
    {ok, Socket} = gen_udp:open(?L_PORT, [binary, {active, true},{recbuf, 0}]),
    {noreply, State#state{socket=Socket}};
handle_info({udp,_Sock,IP,Port,_Data} = FullMsg, State) ->
    lager:debug("PORT: ~p",[Port]),
    WName = {w,IP,Port},
    {ok,Pid} = case gproc:lookup_local_name(WName) of
	undefined ->
		test_tftp_pool_sup:start_child(WName);
	P -> {ok,P}
    end,
 
    lager:debug("PID ~p",[Pid]),
    
    test_tftp_pool_worker:handle_msg(Pid, FullMsg),
    %lager:debug("Got msg: ~p",[byte_size(Data)]),
    {noreply, State};
handle_info(Info, State) ->
    lager:debug("Got msg: ~p",[Info]),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------
%send(Pkg)->
%	gen_server:cast(?MODULE,{send,Pkg}).
