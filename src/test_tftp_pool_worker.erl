-module(test_tftp_pool_worker).
-behaviour(gen_server).
-define(SERVER, ?MODULE).
-define(DEF_ROOT, "/tmp").

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/1]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-export([
	handle_msg/2
]).

-record(state,{
	socket = undefined,
	curr_file = undefined,
	root = undefined
}).
%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link(Name) ->
    gen_server:start_link(?MODULE, [Name], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init([Name]) ->
    gproc:reg({n, l, Name}),
    ROOT = application:get_env(root,test_tftp,?DEF_ROOT),
    {ok, #state{root = ROOT}}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(Msg, #state{root = R, curr_file = CF} = State) ->
    %lager:debug("Got msg to handle: ~p",[Msg]),
    {udp, Sock, IP, Port, Data} = Msg,
    NState = try parse_data(Data, CF) of
	{file, FN} ->
		%lager:debug("!!!~n"),
		send(Sock, IP, Port, FN),
		State;
	{ack_file, D, FN} ->
		FileDst = filename:join(R,binary_to_list(FN)),
		lager:debug("File destination : ~p",[FileDst]),
		case file:open(FileDst,[write]) of
			{ok, F} ->		
				gen_udp:send(Sock,IP,Port,D),
				State#state{curr_file = F};
			_ ->
				gen_udp:send(Sock,IP,Port,err_pkg(2, <<"Error to create file ",(list_to_binary(FileDst))/binary >>)),
				State
		end;
	{ack_eof, D} ->
		%test_tftp_worker:send({IP,Port,D}),
		gen_udp:send(Sock,IP,Port,D),
		State#state{curr_file = undefined};
	{ack, D} ->
		%test_tftp_worker:send({IP,Port,D}),
		gen_udp:send(Sock,IP,Port,D),
		State;
	_ ->
		State
    catch 
	throw:{C,M} ->
		gen_udp:send(Sock,IP,Port,err_pkg(C,M)),
    		{noreply, State};
	_:_ ->
		gen_udp:send(Sock,IP,Port,err_pkg(1,<<"All bad. See log">>)),
		{noreply, State}
    end,
    {noreply, NState}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------
handle_msg(Pid, Msg) ->
	gen_server:cast(Pid, Msg).

parse_data(Data, CF) ->
	<<Opcode:2/integer-unit:8, Block/binary>> = Data,
	RetPkg = case Opcode of
		1 ->
			lager:debug("RRQ got!~n",[]),
			[Filename, Type] = binary:split(Block,[<<0>>],[global,trim]),
			lager:debug("FileName: ~p; Type: ~p~n",[Filename, Type]),
			{file, Filename};
		2 ->
			lager:debug("WRQ got!~n",[]),
			[Filename, Type] = binary:split(Block,[<<0>>],[global,trim]),
			lager:debug("FileName: ~p; Type: ~p~n",[Filename, Type]),
			{ack_file, <<0,4,0,0>>, Filename};
		3 ->
			lager:debug("Data got!~n",[]),
			<<ACK:2/integer-unit:8,D/binary>> = Block,
			%TODO - save file data
			lager:debug("Size: ~p, ACK: ~p",[byte_size(Block),ACK]),
			file:write(CF,D),
			case byte_size(Block) < 514 of
				true -> file:close(CF),
					{ack_eof, <<0,4,ACK:2/integer-unit:8>>};
				false -> 
					{ack, <<0,4,ACK:2/integer-unit:8>>}
			end;
		4 ->
			lager:debug("ACK got!~n",[]),
			lager:debug("Ack code: ~p",[Block]);
		5 ->
			lager:debug("Error got!~n",[]),
			<<ECode:2/integer-unit:8,E>> = Block,
			[Estr] = binary:split(E,[<<0>>],[global,trim]),
			lager:debug("ErrorCode: ~p; ErrStr: ~p~n",[ECode, Estr])
	end,
	RetPkg.

send(Sock, IP, Port, FN) ->
	{ok,FD} = file:open(binary_to_list(FN), [read,binary]),
	send_acc(Sock, IP, Port,FD,1).

send_acc(S,I,P,FD,N) ->
	case file:read(FD,512) of
		{ok, Data} ->
			PData = <<0,3,N:2/integer-unit:8,Data/binary>>,
			Send_Result = gen_udp:send(S,I,P,PData),
			lager:debug("SendResult: ~p, ~p, ~p",[Send_Result, N, byte_size(PData)]),
			send_acc(S,I,P,FD,N+1);
		eof ->
			file:close(FD),
			%gen_udp:send(S,I,P,<<"">>),
			ok;
		_   ->
			throw({1,"Attention!"})
	end.

err_pkg(Code, Msg) ->
	<<0,5,Code:2/integer-unit:8,Msg/binary>>.
